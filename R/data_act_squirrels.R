#' @title DATASET_TITLE
#' @description DATASET_DESCRIPTION
#' @format A data frame with 15 rows and 4 variables:
#' \describe{
#'   \item{\code{age}}{character blalba}
#'   \item{\code{primary_fur_color}}{character blublu}
#'   \item{\code{activity}}{character br}
#'   \item{\code{counts}}{double tr}
#'}
#' @details DETAILS
"data_act_squirrels"
